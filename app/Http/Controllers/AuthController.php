<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('v_formRegis');
    }

    public function welcome(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('v_welcome', ['fname' => $fname,'lname' => $lname]);
    }
}
