<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Registrasi</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form method="POST" action="/welcome">
        @csrf
        <!-- Text: First and Last Name -->
        <p><label for="fname">First Name: </label></p>
        <p><input type="text" id='fname' name="fname"></p>

        <p><label for="lname">Last Name: </label></p>
        <p><input type="text" id='lname' name="lname"></p>

        <!-- Radio: Gender -->
        <p><label for="gender">Gender: </label></p>
        <p><input type="radio" name="gender" id="male"> <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female"> <label for="female">Female</label> <br>
        <input type="radio" name="gender" id="other"> <label for="other">Other</label> </p>

        <!-- Select: Nationality -->
        <p><label for="nationality">Nationality: </label></p>
        <p><select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singaporean">Singaporean</option>
            <option value="australian">Australian</option>
        </select></p>

        <!-- Checkbox: Language Spoken -->
        <label for="spoken">Language Spoken: </label></p>
        <p><input type="checkbox" id="indonesia" value="indonesia"><label for="spoken">Bahasa Indonesia</label><br>
        <input type="checkbox" id="English" value="English"><label for="spoken">English</label><br>
        <input type="checkbox" id="other" value="other"><label for="spoken">Other</label></p>

        <!-- TextArea: Bio -->
        <p><label for="bio">Bio: </label></p>
        <p><textarea name="bio" id="bio" cols="30" rows="10"></textarea></p>

        <!-- Submit -->
        <p><input type="submit" value="Sign Up"></p>
    </form>
</body>
</html>